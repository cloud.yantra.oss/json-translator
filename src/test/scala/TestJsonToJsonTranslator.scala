import cloud.yantra.oss.jsontranslator.CirceHelper
import io.circe.{ACursor, Decoder, HCursor, Json}
import org.scalatest.{BeforeAndAfter, FlatSpecLike, GivenWhenThen, Matchers}
import io.marioslab.basis.template.TemplateContext
import io.marioslab.basis.template.TemplateLoader.ClasspathTemplateLoader

class TestJsonToJsonTranslator
  extends FlatSpecLike
    with Matchers
    with BeforeAndAfter
    with GivenWhenThen {


  val sourceJsonMap = Map(
    "key1" -> "value1",
    "key2/key21" -> "value21",
    "key2/key22" -> "value22",
    "key3/key31" -> "value31",
    "key3/key32/key321" -> "value321",
    "key3/key32/key322" -> "value322",
    "key4" -> "value4"
  )

  "Template Library" must "generate a simple JSON from template " in {
    val ctl = new ClasspathTemplateLoader()
    val template = ctl.load("/target1.bt")
    val context = new TemplateContext()
    context.set("tvalue1", "value1")
    val result = io.circe.parser.parse(template.render(context)).getOrElse(Json.Null)
    val expectedResult = io.circe.parser.parse(
      """
        |{
        |   "tkey1":"value1",
        |   "tkey2":{
        |     "tkey21":"value1",
        |     "tkey22": "value22"
        |   },
        |   "tkey3":{
        |     "tkey31":"value31",
        |     "tkey32": {
        |        "tkey321":"value321",
        |        "tkey322":"value322"
        |     }
        |   },
        |   "tkey4":"value4"
        |}""".stripMargin).getOrElse(Json.Null)
    assert(Json.eqJson.eqv(expectedResult, result))
  }

  it must "generate a simple JSON when an object is passed" in {

    case class Customer(name: String)
    val c = Customer("Fancy Customer")
    val expectedResult = io.circe.parser.parse(
      """
        |{
        |   "tkey1":"value1",
        |   "tkey2":{
        |     "tkey21":"Fancy Customer",
        |     "tkey22": "value22"
        |   },
        |   "tkey3":{
        |     "tkey31":"value31",
        |     "tkey32": {
        |        "tkey321":"value321",
        |        "tkey322":"value322"
        |     }
        |   },
        |   "tkey4":"value4"
        |}
      """.stripMargin).getOrElse(Json.Null)

    val ctl = new ClasspathTemplateLoader()
    val template = ctl.load("/target2.bt")
    val context = new TemplateContext()
    context.set("customer", c)
    val result = io.circe.parser.parse(template.render(context)).getOrElse(Json.Null)
    assert(Json.eqJson.eqv(expectedResult, result))
  }

  it must "generate a JSON when an object handling generics is passed" in {
    val sourceJson = io.circe.parser.parse(
      """
        |{
        |   "key1":"value1",
        |   "key2":{
        |     "key21":"value21",
        |     "key22": "value22"
        |   }
        |}
      """.stripMargin).getOrElse(Json.Null)
    val expectedResult = io.circe.parser.parse(
      """
        |{
        |   "tkey1":"value1",
        |   "tkey2":{
        |     "tkey21":"value21",
        |     "tkey22": "value22"
        |   }
        |}
      """.stripMargin).getOrElse(Json.Null)
    val cursor: HCursor = sourceJson.hcursor

    val ctl = new ClasspathTemplateLoader()
    val template = ctl.load("/target3.bt")
    val context = new TemplateContext()
    context.set("helper", CirceHelper)
    context.set("cursor", cursor)
    context.set("decoder", Decoder.decodeString)
    context.set("default", "Ha Ha! No Value")

    val result = io.circe.parser.parse(template.render(context)).getOrElse(Json.Null)
    assert(Json.eqJson.eqv(expectedResult, result))
  }

  it must "generate a JSON with default value when source does not have the key" in {
    val sourceJson = io.circe.parser.parse(
      """
        |{
        |   "key1":"value1",
        |   "key2":{
        |     "key21":"value21",
        |     "key22": "value22"
        |   }
        |}
      """.stripMargin).getOrElse(Json.Null)
    val expectedResult = io.circe.parser.parse(
      """
        |{
        |   "tkey1":"value1",
        |   "tkey2":{
        |     "tkey21":"Ha Ha! No Value",
        |     "tkey22": "value22"
        |   }
        |}
        |
      """.stripMargin).getOrElse(Json.Null)
    val cursor: HCursor = sourceJson.hcursor
    val ctl = new ClasspathTemplateLoader()
    val template = ctl.load("/target4.bt")
    val context = new TemplateContext()
    context.set("helper", CirceHelper)
    context.set("cursor", cursor)
    context.set("decoder", Decoder.decodeString)
    context.set("default", "Ha Ha! No Value")

    val result = io.circe.parser.parse(template.render(context)).getOrElse(Json.Null)
    assert(Json.eqJson.eqv(expectedResult, result))
  }


  it must "be able to generate JSON when same key is used multiple times" in {
    val sourceJson = io.circe.parser.parse(
      """
        |{
        |   "key1":"value1",
        |   "key2":{
        |     "key21":"value21",
        |     "key22": "value22"
        |   }
        |}
      """.stripMargin).getOrElse(Json.Null)
    val expectedResult = io.circe.parser.parse(
      """
        |{
        |   "tkey1":"value1",
        |   "tkey2":{
        |     "tkey21":"value21",
        |     "tkey22": "value22",
        |     "tkey23": "value21"
        |   }
        |}
      """.stripMargin).getOrElse(Json.Null)
    val cursor: HCursor = sourceJson.hcursor

    val ctl = new ClasspathTemplateLoader()
    val template = ctl.load("/target5.bt")
    val context = new TemplateContext()
    context.set("helper", CirceHelper)
    context.set("cursor", cursor)
    context.set("decoder", Decoder.decodeString)
    context.set("default", "Ha Ha! No Value")

    val result = io.circe.parser.parse(template.render(context)).getOrElse(Json.Null)
    assert(Json.eqJson.eqv(expectedResult, result))
  }

  it must "be able to generate JSON when Number is present" in {
    val sourceJson = io.circe.parser.parse(
      """
        |{
        |   "key1":"value1",
        |   "key2":{
        |     "key21":"value21",
        |     "key22": 10000
        |   }
        |}
      """.stripMargin).getOrElse(Json.Null)
    val expectedResult = io.circe.parser.parse(
      """
        |{
        |   "tkey1":"value1",
        |   "tkey2":{
        |     "tkey21":"value21",
        |     "tkey22": 10000,
        |     "tkey23": "value21"
        |   }
        |}
      """.stripMargin).getOrElse(Json.Null)
    val cursor: HCursor = sourceJson.hcursor

    val ctl = new ClasspathTemplateLoader()
    val template = ctl.load("/target6.bt")
    val context = new TemplateContext()
    context.set("helper", CirceHelper)
    context.set("cursor", cursor)
    context.set("decoderString", Decoder.decodeString)
    context.set("decoderNumber", Decoder.decodeJsonNumber)
    context.set("default", "Ha Ha! No Value")

    val result = io.circe.parser.parse(template.render(context)).getOrElse(Json.Null)
    assert(Json.eqJson.eqv(expectedResult, result))
  }


  it must "be able to generate JSON when Decimal Number is present" in {
    val sourceJson = io.circe.parser.parse(
      """
        |{
        |   "key1":"value1",
        |   "key2":{
        |     "key21":"value21",
        |     "key22": 10000.02
        |   }
        |}
      """.stripMargin).getOrElse(Json.Null)
    val expectedResult = io.circe.parser.parse(
      """
        |{
        |   "tkey1":"value1",
        |   "tkey2":{
        |     "tkey21":"value21",
        |     "tkey22": 10000.02,
        |     "tkey23": "value21"
        |   }
        |}
      """.stripMargin).getOrElse(Json.Null)
    val cursor: HCursor = sourceJson.hcursor

    val ctl = new ClasspathTemplateLoader()
    val template = ctl.load("/target6.bt")
    val context = new TemplateContext()
    context.set("helper", CirceHelper)
    context.set("cursor", cursor)
    context.set("decoderString", Decoder.decodeString)
    context.set("decoderNumber", Decoder.decodeJsonNumber)
    context.set("default", "Ha Ha! No Value")

    val result = io.circe.parser.parse(template.render(context)).getOrElse(Json.Null)
    assert(Json.eqJson.eqv(expectedResult, result))
  }


  it must "be able to generate JSON when Boolean is present" in {
    val sourceJson = io.circe.parser.parse(
      """
        |{
        |   "key1":"value1",
        |   "key2":{
        |     "key21":"value21",
        |     "key22": 10000.02,
        |     "key23": true
        |   }
        |}
      """.stripMargin).getOrElse(Json.Null)
    val expectedResult = io.circe.parser.parse(
      """
        |{
        |   "tkey1":"value1",
        |   "tkey2":{
        |     "tkey21":"value21",
        |     "tkey22": 10000.02,
        |     "tkey23": "value21",
        |     "tkey24": true
        |   }
        |}
      """.stripMargin).getOrElse(Json.Null)
    val cursor: HCursor = sourceJson.hcursor

    val ctl = new ClasspathTemplateLoader()
    val template = ctl.load("/target7.bt")
    val context = new TemplateContext()
    context.set("helper", CirceHelper)
    context.set("cursor", cursor)
    context.set("decoderString", Decoder.decodeString)
    context.set("decoderNumber", Decoder.decodeJsonNumber)
    context.set("decoderBoolean", Decoder.decodeBoolean)
    context.set("default", "Ha Ha! No Value")

    val result = io.circe.parser.parse(template.render(context)).getOrElse(Json.Null)
    assert(Json.eqJson.eqv(expectedResult, result))
  }

  it must "be able to generate JSON when child JSON is present" in {
    val sourceJson = io.circe.parser.parse(
      """
        |{
        |   "key1":"value1",
        |   "key2":{
        |     "key21":"value21",
        |     "key22": 10000.02,
        |     "key23": true,
        |     "key24":{
        |       "key241": "value241",
        |       "key242": 1000,
        |       "key243": false
        |     }
        |   }
        |}
      """.stripMargin).getOrElse(Json.Null)
    val expectedResult = io.circe.parser.parse(
      """
        |{
        |   "tkey1":"value1",
        |   "tkey2":{
        |     "tkey21":"value21",
        |     "tkey22": 10000.02,
        |     "tkey23": "value21",
        |     "tkey24": true,
        |     "tkey25": {
        |       "key241": "value241",
        |       "key242": 1000,
        |       "key243": false
        |     }
        |   }
        |}
      """.stripMargin).getOrElse(Json.Null)
    val cursor: HCursor = sourceJson.hcursor

    val ctl = new ClasspathTemplateLoader()
    val template = ctl.load("/target8.bt")
    val context = new TemplateContext()
    context.set("helper", CirceHelper)
    context.set("cursor", cursor)
    context.set("decoderString", Decoder.decodeString)
    context.set("decoderNumber", Decoder.decodeJsonNumber)
    context.set("decoderBoolean", Decoder.decodeBoolean)
    context.set("decoderJson", Decoder.decodeJson)
    context.set("defaultString", "Ha Ha! No Value")
    context.set("defaultNumber", "-1000")
    context.set("defaultBoolean", "false")
    context.set("defaultJson", "{}")

    val rs = template.render(context)
    val result = io.circe.parser.parse(rs).getOrElse(Json.Null)
    assert(Json.eqJson.eqv(expectedResult, result))
  }

  it must "be able to generate JSON when Array is present" in {
    val sourceJson = io.circe.parser.parse(
      """
        |{
        |   "key1":"value1",
        |   "key2":{
        |     "key21":"value21",
        |     "key22": 10000.02,
        |     "key23": true,
        |     "key24": [1,2,3,4]
        |   }
        |}
      """.stripMargin).getOrElse(Json.Null)
    val expectedResult = io.circe.parser.parse(
      """
        |{
        |   "tkey1":"value1",
        |   "tkey2":{
        |     "tkey21":"value21",
        |     "tkey22": 10000.02,
        |     "tkey23": "value21",
        |     "tkey24": true,
        |     "tkey25": [1,2,3,4]
        |   }
        |}
      """.stripMargin).getOrElse(Json.Null)
    val cursor: HCursor = sourceJson.hcursor

    val ctl = new ClasspathTemplateLoader()
    val template = ctl.load("/target8.bt")
    val context = new TemplateContext()
    context.set("helper", CirceHelper)
    context.set("cursor", cursor)
    context.set("decoderString", Decoder.decodeString)
    context.set("decoderNumber", Decoder.decodeJsonNumber)
    context.set("decoderBoolean", Decoder.decodeBoolean)
    context.set("decoderJson", Decoder.decodeJson)
    context.set("defaultString", "Ha Ha! No Value")
    context.set("defaultNumber", "-1000")
    context.set("defaultBoolean", "false")
    context.set("defaultJson", "{}")

    val rs = template.render(context)
    val result = io.circe.parser.parse(rs).getOrElse(Json.Null)
    assert(Json.eqJson.eqv(expectedResult, result))
  }

  it must "be able to generate JSON when complex child JSON is present" in {
    val sourceJson = io.circe.parser.parse(
      """
        |{
        |   "key1":"value1",
        |   "key2":{
        |     "key21":"value21",
        |     "key22": 10000.02,
        |     "key23": true,
        |     "key24":{
        |       "key241": "value241",
        |       "key242": 1000,
        |       "key243": false,
        |       "key244": [1,2,3,4],
        |       "key245": {
        |         "key2451": "value2451",
        |         "key2452": [4,5,6,7],
        |         "key2452": ["value24521", "value24522", "value24523"]
        |        }
        |     }
        |   }
        |}
      """.stripMargin).getOrElse(Json.Null)
    val expectedResult = io.circe.parser.parse(
      """
        |{
        |   "tkey1":"value1",
        |   "tkey2":{
        |     "tkey21":"value21",
        |     "tkey22": 10000.02,
        |     "tkey23": "value21",
        |     "tkey24": true,
        |     "tkey25": {
        |       "key241": "value241",
        |       "key242": 1000,
        |       "key243": false,
        |       "key244": [1,2,3,4],
        |       "key245": {
        |         "key2451": "value2451",
        |         "key2452": [4,5,6,7],
        |         "key2452": ["value24521", "value24522", "value24523"]
        |        }
        |     }
        |   }
        |}
      """.stripMargin).getOrElse(Json.Null)
    val cursor: HCursor = sourceJson.hcursor

    val ctl = new ClasspathTemplateLoader()
    val template = ctl.load("/target8.bt")
    val context = new TemplateContext()
    context.set("helper", CirceHelper)
    context.set("cursor", cursor)
    context.set("decoderString", Decoder.decodeString)
    context.set("decoderNumber", Decoder.decodeJsonNumber)
    context.set("decoderBoolean", Decoder.decodeBoolean)
    context.set("decoderJson", Decoder.decodeJson)
    context.set("defaultString", "Ha Ha! No Value")
    context.set("defaultNumber", "-1000")
    context.set("defaultBoolean", "false")
    context.set("defaultJson", "{}")

    val rs = template.render(context)
    val result = io.circe.parser.parse(rs).getOrElse(Json.Null)
    assert(Json.eqJson.eqv(expectedResult, result))
  }

}