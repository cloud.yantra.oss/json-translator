package cloud.yantra.oss.jsontranslator

import io.circe.{ACursor, Decoder}

object CirceHelper {
  def extractString[T](cursor: ACursor,
                       decoder: Decoder[T],
                       default: T): T =
    cursor.as[T](decoder).getOrElse(default)

}
