import sbt._

object Dependencies {

  lazy val scalatestVersion = "3.0.5"
  lazy val scalamockVersion = "4.1.0"
  lazy val circeVersion = "0.11.1"

  // Library for JSON parsing
  val circeCore = "io.circe" %% "circe-core" % circeVersion
  val circeGeneric = "io.circe" %% "circe-generic" % circeVersion
  val circeParser = "io.circe" %% "circe-parser" % circeVersion
//  val circeLiteral = "io.circe" %% "circe-literal" % circeVersion

  // Templating engine
  val template = "io.marioslab.basis" % "template" % "1.7"

  //Libraries for Testing
  val scalatest = "org.scalatest" %% "scalatest" % scalatestVersion % Test
  val scalamock = "org.scalamock" %% "scalamock" % scalamockVersion % Test

  //Projects
  val groupBackendDependencies = Seq(
    circeCore,
    circeGeneric,
    circeParser,
//    circeLiteral,
    template,
    scalatest,
    scalamock
  )
}
