// The Typesafe repository
resolvers += "Typesafe repository" at "https://repo.typesafe.com/typesafe/maven-releases/"

// for autoplugins
addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.3.19" withSources())

// Java checkstyle
addSbtPlugin("com.etsy" % "sbt-checkstyle-plugin" % "3.1.1")

dependencyOverrides += "com.puppycrawl.tools" % "checkstyle" % "8.12"

// Scala checkstyle
addSbtPlugin("org.scalastyle" % "scalastyle-sbt-plugin" % "1.0.0")

//Sonar Scanner
addSbtPlugin("com.olaq" % "sbt-sonar-scanner-plugin" % "1.3.0")

// Jacoco Plugin
addSbtPlugin("com.github.sbt" % "sbt-jacoco" % "3.1.0")

dependencyOverrides ++= Seq( "org.jacoco" % "org.jacoco.core" % "0.8.3", "org.jacoco" % "org.jacoco.report" % "0.8.3")
