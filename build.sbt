import com.typesafe.sbt.packager.docker.{Cmd, ExecCmd}

/** ****************************************************************************
  * JSON Translator Library
  * ****************************************************************************
  */

enablePlugins(UniversalPlugin)

/** ****************************************************************************
  * Application related configurations
  * ****************************************************************************
  */
organization := "cloud.yantra.oss"
name := "JSON Translator"
packageName := "json-translator"

/** ****************************************************************************
  * Compilation related
  * ****************************************************************************
  */

scalaVersion := "2.12.8"
scalacOptions ++= Seq("-target:jvm-1.8",
  "-unchecked",
  "-deprecation",
  "-encoding", "utf8",
  "-feature",
  "-Ywarn-adapted-args",
  "-Ywarn-dead-code")

javacOptions in(Compile, compile) ++= Seq("-source", "11",
  "-target", "11",
  "-g")


logLevel := sbt.Level.Warn
libraryDependencies ++= Dependencies.groupBackendDependencies

/** ****************************************************************************
  * CI : Scala Checkstyle
  * Ref: http://www.scalastyle.org/sbt.html
  * Usage: sbt scalastyle
  * ****************************************************************************
  */
lazy val scalaCheckstyle = "ci/checkstyle/scala/scalastyle-config.xml"
scalastyleConfig := baseDirectory(_ / scalaCheckstyle).value
scalastyleFailOnWarning := true


/** ****************************************************************************
  * CI : Pipeline Simulation
  * Usage: sbt pipeline-ci
  * ****************************************************************************
  */
commands += Command.command("pipeline-ci") { state =>
  "clean" ::
    "compile" ::
    "test" ::
    state
}

// jacoco reporting set to XML format
jacocoReportSettings := JacocoReportSettings()
  .withThresholds(
    JacocoThresholds(
      instruction = 80,
      method = 60,
      branch = 50,
      complexity = 60,
      line = 60,
      clazz = 60
    )
  )
  .withTitle("Report - JSON Translator")
  .withFormats(JacocoReportFormats.XML, JacocoReportFormats.HTML)

/** ****************************************************************************
  * Packaging related configurations
  * ****************************************************************************
  */

packageName in Universal := s"${packageName.value}-${version.value}"
exportJars := true

//By default, the dist task will include the API documentation in the generated package.
//Below instruction will exclude them/
sources in(Compile, doc) := Seq.empty
publishArtifact in(Compile, packageDoc) := false
